from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

def run():
    """requirements for task 1E"""

    # Build list of stations
    stations = build_station_list()

    print(rivers_by_station_number(stations, 9))

if __name__ == "__main__":
    run()
from floodsystem.analysis import polyfit
import matplotlib.dates as mpdates
from datetime import datetime
from dateutil.tz import tzutc
import numpy

def test_polyfit():
    
    # Create datetime equivalents of 1, 2, 3, 4
    dates = [datetime(1, 1, 1, 0, 0, tzinfo=tzutc()), datetime(1, 1, 2, 0, 0, tzinfo=tzutc()), datetime(1, 1, 3, 0, 0, tzinfo=tzutc()), datetime(1, 1, 4, 0, 0, tzinfo=tzutc())]
    
    # Create levels that match equations of x^3 - 3x + 4 or (x-1)^3 + 3(x-1)^2 + 2
    levels = [2, 6, 22, 56]

    polydata = polyfit(dates, levels, 3)

    assert type(polydata) == tuple
    assert type(polydata[0]) == numpy.poly1d
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from floodsystem.station import MonitoringStation

num1 = 100
num2 = 100
coord = (100,100)

stations_for_test = [
    MonitoringStation(num1, num2, "station1", coord, None, "river1", "town1"),
    MonitoringStation(num1, num2, "station2", coord, (0.1, 3), "river2", "town2"),
    MonitoringStation(num1, num2, "station3", coord, (3, 0.1), "river3", "town3"),
    MonitoringStation(num1, num2, "station4", coord, (0, 1), "river4", "town4"),
    MonitoringStation(num1, num2, "station5", coord, None, "river5", "town5"),
    MonitoringStation(num1, num2, "station6", coord, (0, 1), "river6", "town6")
]

stations_for_test[0].latest_level = 12.
stations_for_test[1].latest_level = 3.
stations_for_test[2].latest_level = 54.
stations_for_test[3].latest_level = 2.
stations_for_test[4].latest_level = 34.
stations_for_test[5].latest_level = 0.7


def test_stations_level_over_threshold():
    outputs = stations_level_over_threshold(stations_for_test, 1)
    assert (stations_for_test[1], 1) not in outputs
    assert (stations_for_test[3], 2) in outputs
    assert (stations_for_test[5], 0.7) not in outputs


def test_stations_highest_rel_level():
    highest_stations = stations_highest_rel_level(stations_for_test, 3)
    print(len(highest_stations))
    assert len(highest_stations) == 3
    for i in range(len(highest_stations)-1):
        assert highest_stations[i+1].relative_water_level() < highest_stations[i].relative_water_level()


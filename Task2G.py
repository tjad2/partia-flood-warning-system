from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations
from floodsystem.geo import stations_by_distance, stations_within_radius, rivers_with_station, stations_by_river, rivers_by_station_number
from floodsystem.datafetcher import fetch_measure_levels, fetch_latest_water_level_data, fetch_station_data
import datetime
import matplotlib.dates as mpdates
from dateutil.tz import tzutc
from floodsystem.analysis import polyfit


def run():
    """Requirements for Task 2G"""

    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    ### Our function for comparison will be called 'risk'
    ### It will be determined in two stages, using current relative level, and rate of increase of current level
    ### Severe risk:        risk>1
    ### High risk:          1>risk>0.8
    ### Moderate risk:      0.8>risk>0.65
    ### Low risk:           0.65>risk

    station_risk = []
    severe_risk = {}
    high_risk = {}
    moderate_risk = {}
    low_risk = {}

    for i in stations:
        
        if i.relative_water_level() != None:

            risk = i.relative_water_level()
        
        
            # Calculates a level gradient by taking a linear approximation of readings from the last two days
            levels = []
            dates = []
            dates_levels = fetch_measure_levels(i.measure_id, dt=datetime.timedelta(days=2))
            for j in range(len(dates_levels[0])):
                dates.append(dates_levels[0][j])
                levels.append(dates_levels[1][j])

            if polyfit(dates, levels, 1) != None:
                level_gradient = polyfit(dates, levels, 1)[0][1]

            risk += level_gradient/(i.typical_range[1] - i.typical_range[0])

            # Classifying towns by risk
            # If town is repeated, take the highest risk level

            if risk >= 1:
                risk_class = "Severe"
                if i.town in severe_risk:
                    if risk > severe_risk[i.town]:
                        severe_risk[i.town] = risk
                else:
                    severe_risk[i.town] = risk
            elif 1 > risk >= 0.8:
                risk_class = "High"
                if i.town in high_risk:
                    if risk > high_risk[i.town]:
                        high_risk[i.town] = risk
                else:
                    high_risk[i.town] = risk
            elif 0.8 > risk > 0.65:
                risk_class = "Moderate"
                if i.town in moderate_risk:
                    if risk > moderate_risk[i.town]:
                        moderate_risk[i.town] = risk
                else:
                    moderate_risk[i.town] = risk
            else:
                risk_class = "Low"
                if i.town in low_risk:
                    if risk > low_risk[i.town]:
                        low_risk[i.town] = risk
                else:
                    low_risk[i.town] = risk

            station_risk.append((i, risk_class))


    print(severe_risk)

if __name__ == "__main__":
    run()
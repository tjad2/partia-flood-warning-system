from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.utils import sorted_by_key
from floodsystem.station import MonitoringStation

def stations_level_over_threshold(stations : MonitoringStation, tol: float): #-> List[Tuple[MonitoringStation, float]]
    return sorted_by_key([(station, station.relative_water_level()) for station in stations if station.relative_water_level() != None and station.relative_water_level() > tol], 1, True)

def stations_level_over_threshold1(stations, tol):
    """returns a list of tuples, where each tuple holds:
    (i) a station (object) at which the latest relative water level is over tol 
    (ii) the relative water level at the station
    sorted by the relative level in descending order."""

    stations_over_threshold = []
    update_water_levels(stations)

    for i in stations:
        if type(i.relative_water_level()) == float:
            if i.relative_water_level() > tol:
                stations_over_threshold.append((i, i.relative_water_level()))
            else:
                pass
        else:
            pass 

    return sorted_by_key(stations_over_threshold, 1, reverse=True)

def stations_highest_rel_level(stations : MonitoringStation, N : int):
    """returns a list of the N stations (objects) at which the water level, relative to the typical range, is highest.
     The list should be sorted in descending order by relative level"""
    return sorted([station for station in stations if station.relative_water_level() != None], key = lambda x: x.relative_water_level(), reverse = True)[:N]


def stations_highest_rel_level1(stations, N):
    """returns a list of the N stations (objects) at which the water level, relative to the typical range, is highest.
     The list should be sorted in descending order by relative level"""

    #make list of all stations and relative water level
    relative_levels = []
    update_water_levels(stations)

    for i in stations:
        if type(i.relative_water_level()) == float:
                relative_levels.append((i, i.relative_water_level()))
        else:
            pass

    #return first N stations from sorted list
    return(sorted_by_key(relative_levels, 1, reverse=True)[:N])


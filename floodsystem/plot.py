import matplotlib
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from .datafetcher import fetch_measure_levels
from .station import MonitoringStation
from .analysis import polyfit
import numpy as np

def plot_water_levels(station, dates, levels):
    """uses data for a time period given by dates for a station
    and plots it on a graph"""

    if len(dates) != len(levels):
        raise IndexError("Error - different number of dates to number of readings")
    
    # Plot
    plt.plot(dates, levels)

    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(str(station.name))

    typical_min = [station.typical_range[0]] * len(dates)
    typical_max = [station.typical_range[1]] * len(dates)

    plt.plot(dates, typical_min)
    plt.plot(dates, typical_max)

    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels

    plt.show()


def plot_water_level_with_fit(station, dates, levels, p):
    """plots the water level data and the best-fit polynomial"""

    poly, offset = polyfit(dates, levels, p)[0],polyfit(dates, levels, p)[1]

    x = matplotlib.dates.date2num(dates)

    # Plot original data points
    plt.plot(dates, levels, '.')

    # Plot polynomial fit at points along interval (note that polynomial
    # is evaluated using the shift x)
    x1 = np.linspace(x[0], x[-1], 300)
    plt.plot(x1, poly(x1 - offset))
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation = 45)
    plt.title(str(station.name))

    # Display plot
    plt.show()
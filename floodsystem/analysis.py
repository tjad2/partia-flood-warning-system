import numpy as np
import matplotlib.pyplot as plt
import matplotlib


def polyfit(dates, levels, p):
    """Given the water level time history (dates, levels) for a station computes a least-squares fit of a polynomial of degree p to water level data
    
    Returns a tuple of (i) the polynomial object and (ii) any shift of the time (date) axis"""

    if len(dates) == 0:
        #raise TypeError("dates input is a zero-length list")
        return None
    if len(dates) != len(levels):
        #raise TypeError("must have same number of dates as levels")
        return None

    # Converts dates to floats
    x = matplotlib.dates.date2num(dates)

    # Using shifted dates values, find coefficient of best-fit
    # polynomial f(dates) of degree p
    p_coeff = np.polyfit(x - x[0], levels, p)

    # Return the polynomial generated (for points f(x-x[0]))
    return (np.poly1d(p_coeff), x[0])
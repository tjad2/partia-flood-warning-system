# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key #noqa
from haversine import haversine

def stations_by_distance(stations, p):
    """ Given a list of station objects and a coordinate tuple 'p', 
    returns a list of (station, distance) tuples, where distance (float)
    is the distance of the station (MonitoringStation) from the 
    coordinate p. The returned list is sorted by distance.
    
    """
    

    ### Checking the types of data entered for p and stations ###

    if type(p) != tuple:
        raise TypeError("p should be the coordinates of the point the distances are measured from in the form of a Tuple. Your entry was not a Tuple.")
    if len(p) != 2:
        raise TypeError("p should be the coordinates of the point the distances are measured from in the form of a Tuple. Your entry was not length 2, so may cause issues in being recognised as a coordinate.")
    
    if type(stations) != list:
        raise TypeError("stations should be a list of the station objects that you wish to test. Your entry was not a list.")

    distances = []
    
    # Populates distances with the stations and their corresponding distances

    for i in stations:
        r = haversine(i.coord, p)
        distances.append((i, r))

    # Returns the list of distances after sorting it according to the second index (the distance)
    
    return sorted_by_key(distances, 1)


def stations_within_radius(stations, centre, r):
    """ Returns a list of all stations (type MonitoringStation) within radius 'r' of a geographic coordinate 'centre' """

    # Checking the types of data entered for centre, r and stations 

    if type(centre) != tuple:
        raise TypeError("centre should be the coordinates of the point the distances are measured from in the form of a Tuple. Your entry was not a Tuple.")
    if len(centre) != 2:
        raise TypeError("centre should be the coordinates of the point the distances are measured from in the form of a Tuple. Your entry was not length 2, so may cause issues in being recognised as a coordinate.")
    
    if type(r) != float:
        if type(r) != int:
            raise TypeError("r should be a single float or an int")
    
    if type(stations) != list:
        raise TypeError("stations should be a list of the station objects that you wish to test. Your entry was not a list.")


    # Create a list of stations with their corresponding distances to the centre

    distances_list = stations_by_distance(stations = stations, p = centre)

    # Add close enough stations to a new list

    close_stations = []

    for i in distances_list:
        if i[1] <= r:
            close_stations.append(i[0])

    return close_stations


def rivers_with_station(stations):
    """  given a list of station objects, returns a set with the names of the rivers with a monitoring station """

    # checking the type of data entered for stations

    if type(stations) != list:
        raise TypeError("stations should be a list of objects to test. Entry was not a list.")

    # Add rivers with stations to a set

    rivers_with_station = set()

    for i in stations:
        rivers_with_station.add(str(i.river))

    return rivers_with_station


def stations_by_river(stations):
    """returns a dictionary that maps river names (the ‘key’) to a list of station objects on a given river"""

     # checking the type of data entered for stations

    if type(stations) != list:
        raise TypeError("stations should be a list of objects to test. Entry was not a list.")

    #create empty dictionary

    stations_by_river = {}

    #add entries to dictionary 

    for i in stations:
        if i.river not in stations_by_river:
            stations_by_river[i.river] = [i]
        else:
            stations_by_river[i.river].append(i) 

    return stations_by_river


def rivers_by_station_number(stations, N):
    """determines the N rivers with the greatest number of monitoring stations. 
    Returns a list of (river name, number of stations) tuples, sorted by the number of stations. 
    In the case that there are more rivers with the same number of stations as the N th entry, includes these rivers in the list."""

   # checking the type of data entered for stations
   
    if type(stations) != list:
        raise TypeError("stations should be a list of objects to test. Entry was not a list.")

    # checking the type of data entered for N

    if type(N) != int:
        raise TypeError("N should be an interger number of rivers to be shown. Entry was not an interger")

    #make a dictionary of of all rivers and their number of stations

    station_number = {}

    for i in stations:

        if i.river in station_number:
            station_number[i.river] += 1
        
        else:
             station_number[i.river] = 1

    #make a sorted list from the dictionary

    station_number_sorted = []

    for key, value in sorted(station_number.items(), key=lambda item: item[1]):
        station_number_sorted.append((key, value))

    #reverse list order to get high to low

    station_number_sorted.reverse()
    #returning N entries off list
    if N >= len(station_number_sorted):
        return station_number_sorted
    while N+1 < len(station_number_sorted) and station_number_sorted[N][1] == station_number_sorted[N+1][1]:
        N += 1
        print("yeet")
    return station_number_sorted[:N+1]

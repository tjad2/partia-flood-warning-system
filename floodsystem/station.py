# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d


    def typical_range_consistent(self):
        """ Checks the typical high/low range data for consistency. 
        
        The method should return True if the data is consistent and False if the data is inconsistent or unavailable"""

        consistency = False

        if type(self.typical_range) != tuple:
            return False
        elif self.typical_range[1] - self.typical_range[0] > 0:
            consistency = True
        else:
            consistency = False

        return consistency


    def relative_water_level(self):
        """ returns the latest water level as a fraction of the typical range,
        a ratio of 1.0 corresponds to a level at the typical high and a ratio of 0.0 corresponds to a level at the typical low
        If the necessary data is not available or is inconsistent, the function should return None"""

        #check consistency and availability 
        if self.typical_range_consistent() == False:
            return None
        elif self.typical_range == None:
            return None
        elif self.latest_level == None:
            return None
        
        #calculate fraction
        else:
            typical_range_size = self.typical_range[1] - self.typical_range[0]
            water_level_fraction = (self.latest_level - self.typical_range[0]) / typical_range_size

        return water_level_fraction



def inconsistent_typical_range_stations(stations):
    """ Given a list of stations, returns those with inconsistent typical range data, using typical_range_consistent to 
    determine inconsistent stations """

    inconsistent = []

    for i in stations:
        if i.typical_range_consistent() == False:
            inconsistent.append(i)


    return inconsistent

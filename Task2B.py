from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold
import numpy as np

def run():
 """Requirements for Task 2B"""

    # Build list of stations
stations = build_station_list()

update_water_levels(stations)

[print(a.name, b) for a, b in stations_level_over_threshold(stations, 0.8)]



if __name__ == "__main__":
    run()
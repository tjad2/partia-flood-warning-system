from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list


def run():
    """Requirements for Task 1B"""

    # Build list of stations
    stations = build_station_list()

    # Use stations_by_distance to create the list of distances
    camb_coords = (52.2053, 0.1218)
    distances = stations_by_distance(stations = stations, p = camb_coords)

    # Find the closest 10 stations
    closest = distances[:10]

    # Find the furthest 10 stations
    furthest = distances[-10:]

    print("The 10 closest stations are:")

    for i in closest:
        print(str(i[0].name) + ", " + str(i[0].town) + ", " + str(i[1]))

    print(
        "----------------------------------"
        
    )

    print("The 10 furthest stations are:")

    for i in furthest:
        print(str(i[0].name) + ", " + str(i[0].town) + ", " + str(i[1]))

if __name__ == "__main__":
    run()
from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations

def run():
    """Requirements for Task 1F"""

    # Build list of stations
    stations = build_station_list()

    # Extract only the inconsistent stations
    inconsistent_stations = inconsistent_typical_range_stations(stations)

    # Create list of names
    names_list = []

    for i in inconsistent_stations:
        names_list.append(i.name)

    # Print names of inconsistent stations, which are alphabetically sorted
    print(sorted(names_list))


if __name__ == "__main__":
    run()

    
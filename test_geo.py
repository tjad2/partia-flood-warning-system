from floodsystem.geo import stations_by_distance, stations_within_radius, rivers_with_station, stations_by_river, rivers_by_station_number
from floodsystem.station import MonitoringStation

def test_stations_by_distance():

    # Create test stations
    near_id = "near-id"
    mid_id = "mid-id"
    far_id = "far-id"

    near_coord = (1.0, 0.0)
    mid_coord = (0.0, 2.0)
    far_coord = (2.0, -2.0)

    m_id = "test-m-id"
    label = "some station"
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    
    nearstation = MonitoringStation(near_id, m_id, label, near_coord, trange, river, town)
    midstation = MonitoringStation(mid_id, m_id, label, mid_coord, trange, river, town)
    farstation = MonitoringStation(far_id, m_id, label, far_coord, trange, river, town)

    stationlist = [nearstation, farstation, midstation]

    coord = (0.0, 0.0)

    assert stations_by_distance(stationlist, coord)[0][0] == nearstation
    assert stations_by_distance(stationlist, coord)[1][0] == midstation
    assert stations_by_distance(stationlist, coord)[2][0] == farstation
    

def test_stations_within_radius():

    # Create test stations
    near_id = "near-id"
    mid_id = "mid-id"
    far_id = "far-id"

    near_coord = (1.0, 0.0)
    mid_coord = (0.0, 2.0)
    far_coord = (2.0, -2.0)

    m_id = "test-m-id"
    label = "some station"
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    
    nearstation = MonitoringStation(near_id, m_id, label, near_coord, trange, river, town)
    midstation = MonitoringStation(mid_id, m_id, label, mid_coord, trange, river, town)
    farstation = MonitoringStation(far_id, m_id, label, far_coord, trange, river, town)

    stationlist = [nearstation, farstation, midstation]

    coord = (0.0, 0.0)
    radius = 250

    assert stations_within_radius(stationlist, coord, radius) == [nearstation, midstation]


def test_rivers_with_station():
    
     # Create test stations

    near_id = "near-id"

    near_coord = (1.0, 0.0)

    m_id = "test-m-id"
    label = "some station"
    trange = (-2.3, 3.4445)
    town = "My Town"

    river1 = "river1"
    river2 = "river2"

    station1 = MonitoringStation(near_id, m_id, label, near_coord, trange, river1, town)
    station2 = MonitoringStation(near_id, m_id, label, near_coord, trange, river2, town)
    station3 = MonitoringStation(near_id, m_id, label, near_coord, trange, river2, town)

    stations = [station1, station2, station3]

    assert rivers_with_station(stations) == {river1, river2}


def test_stations_by_river():

      # Create test stations

    near_id = "near-id"

    near_coord = (1.0, 0.0)

    m_id = "test-m-id"
    label = "some station"
    trange = (-2.3, 3.4445)
    town = "My Town"

    river1 = "river1"
    river2 = "river2"

    station1 = MonitoringStation(near_id, m_id, label, near_coord, trange, river1, town)
    station2 = MonitoringStation(near_id, m_id, label, near_coord, trange, river2, town)
    station3 = MonitoringStation(near_id, m_id, label, near_coord, trange, river2, town)

    stations = [station1, station2, station3]

    assert stations_by_river(stations)[river1] == [station1]
    assert stations_by_river(stations)[river2] == [station2, station3]


def test_rivers_by_station_number():

    # Create test stations

    near_id = "near-id"

    near_coord = (1.0, 0.0)

    m_id = "test-m-id"
    label = "some station"
    trange = (-2.3, 3.4445)
    town = "My Town"

    river1 = "river1"
    river2 = "river2"

    station1 = MonitoringStation(near_id, m_id, label, near_coord, trange, river1, town)
    station2 = MonitoringStation(near_id, m_id, label, near_coord, trange, river2, town)
    station3 = MonitoringStation(near_id, m_id, label, near_coord, trange, river2, town)

    stations = [station1, station2, station3]

    assert rivers_by_station_number(stations, 2) == [(river2, 2), (river1, 1)]
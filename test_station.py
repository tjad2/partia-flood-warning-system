# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the station module"""

from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations



def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town
    assert s.typical_range_consistent() == True


def test_inconsistent_typical_range_stations():

    # Create stations to test
    truestation_id = "True Station"
    truestation_levels = (0, 1)

    reversedstation_id = "Reversed Station"
    reversedstation_levels = (1, 0)

    nonestation_id = "None Station"
    nonestation_levels = None

    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    river = "River X"
    town = "My Town"

    truestation = MonitoringStation(truestation_id, m_id, label, coord, truestation_levels, river, town)
    reversedstation = MonitoringStation(reversedstation_id, m_id, label, coord, reversedstation_levels, river, town)
    nonestation = MonitoringStation(nonestation_id, m_id, label, coord, nonestation_levels, river, town)

    stationlist = [truestation, reversedstation, nonestation]

    assert inconsistent_typical_range_stations(stationlist) == [reversedstation, nonestation]


from floodsystem.plot import plot_water_level_with_fit, plot_water_levels
from floodsystem.station import MonitoringStation
from datetime import datetime, timedelta
from dateutil.tz import tzutc

def test_plot_water_levels():
    
    station = MonitoringStation("station-id", "measure-id", "label", (0.1, 0,2), (0.3, 1.5), "river", "town")
    station.name = "station name"
    levels = [0.35, 0.38, 0.44, 0.45, 0.40, 0.38, 0.55, 0.76, 1.02, 1.00]
    dates = [datetime(2020, 2, 12, 12, 15, tzinfo=tzutc()), datetime(2020, 2, 12, 12, 0, tzinfo=tzutc()), datetime(2020, 2, 12, 11, 45, tzinfo=tzutc()), datetime(2020, 2, 12, 11, 30, tzinfo=tzutc()), datetime(2020, 2, 12, 11, 15, tzinfo=tzutc()), datetime(2020, 2, 12, 11, 0, tzinfo=tzutc()), datetime(2020, 2, 12, 10, 45, tzinfo=tzutc()), datetime(2020, 2, 12, 10, 30, tzinfo=tzutc()), datetime(2020, 2, 12, 10, 15, tzinfo=tzutc()), datetime(2020, 2, 12, 10, 0, tzinfo=tzutc())]

    plot_water_levels(station, dates, levels)

def test_plot_water_level_with_fit():

    station = MonitoringStation("station-id", "measure-id", "label", (0.1, 0,2), (0.3, 1.5), "river", "town")
    station.name = "station name"
    levels = [0.35, 0.38, 0.44, 0.45, 0.40, 0.38, 0.55, 0.76, 1.02, 1.00]
    dates = [datetime(2020, 2, 12, 12, 15, tzinfo=tzutc()), datetime(2020, 2, 12, 12, 0, tzinfo=tzutc()), datetime(2020, 2, 12, 11, 45, tzinfo=tzutc()), datetime(2020, 2, 12, 11, 30, tzinfo=tzutc()), datetime(2020, 2, 12, 11, 15, tzinfo=tzutc()), datetime(2020, 2, 12, 11, 0, tzinfo=tzutc()), datetime(2020, 2, 12, 10, 45, tzinfo=tzutc()), datetime(2020, 2, 12, 10, 30, tzinfo=tzutc()), datetime(2020, 2, 12, 10, 15, tzinfo=tzutc()), datetime(2020, 2, 12, 10, 0, tzinfo=tzutc())]

    plot_water_level_with_fit(station, dates, levels, 5)

test_plot_water_levels()
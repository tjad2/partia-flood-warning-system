from floodsystem.plot import plot_water_level_with_fit
from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_highest_rel_level
from floodsystem.datafetcher import fetch_measure_levels
import datetime


def run():
    """requirements for Task 2F"""

    # Build a list of stations
    stations = build_station_list()
    highwater_stations = stations_highest_rel_level(stations, 5)

    for i in highwater_stations:
        levels = []
        dates = []
        dates_levels = fetch_measure_levels(i[0].measure_id, dt=datetime.timedelta(days=10))
        for j in range(len(dates_levels[0])):
            dates.append(dates_levels[0][j])
            levels.append(dates_levels[1][j])
        plot_water_level_with_fit(i[0], dates, levels, p = 4)

if __name__ == "__main__":
    run()
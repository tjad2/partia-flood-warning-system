from floodsystem.geo import stations_by_distance, stations_within_radius
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for Task 1C"""

    # Build list of stations
    stations = build_station_list()

    # Use stations_within_radius to create the list of stations close enough
    camb_coords = (52.2053, 0.1218)
    within_radius = stations_within_radius(stations = stations, centre = camb_coords, r = 10)

    names_list = []

    for i in within_radius:
        names_list.append(i.name)

    print(sorted(names_list))

if __name__ == "__main__":
    run()
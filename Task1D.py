from floodsystem.geo import rivers_with_station, stations_by_river
from floodsystem.stationdata import build_station_list

def run():
    """requirements for task 1D"""

    # Build list of stations
    stations = build_station_list()
    
    #print number of rivers with monitoring stations 

    print(len(rivers_with_station(stations)))

    #print first 10 rivers

    ordered_stations = sorted(rivers_with_station(stations))
    print(ordered_stations[0:9])

    #make lists of stations on each river then sort and print
    
    stations_dict = stations_by_river(stations)
    aire_stations = stations_dict["River Aire"]
    cam_stations = stations_dict["River Cam"]
    thames_stations = stations_dict["River Thames"]

    print(sorted([station.name for station in aire_stations]), 
        sorted([station.name for station in cam_stations]), 
        sorted([station.name for station in thames_stations]))


if __name__ == "__main__":
    run()


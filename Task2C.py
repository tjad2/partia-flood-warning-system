from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level

def run():
    """requirements for task 2C"""

    # Build list of stations
    stations = build_station_list()
    update_water_levels(stations)

    [print(station.name, station.relative_water_level()) for station in stations_highest_rel_level(stations, 10)]

if __name__ == "__main__":
    run()